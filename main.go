package main

import (
	"io"
	"os"
	"strconv"
	"time"

	"github.com/hajimehoshi/oto"
	"github.com/pkg/errors"
	"github.com/tosone/minimp3"
	"gitlab.com/home_life_management/common_lib"
	"gitlab.com/home_life_management/common_lib/service/database"
	"gitlab.com/home_life_management/common_lib/service/model"
	"gitlab.com/home_life_management/common_lib/service/switchbot"
	"gitlab.com/home_life_management/light_management_night/config"
	"go.uber.org/zap"
)

type LightManager interface {
	GetLightedMinsAfter(time.Time, int) (int, error)
	GetLatestRpzSensorLog() (model.RpzSensorLog, error)
	SetSwitchBotStatus(macAddress string, on bool) error
	GetSwitchBotStatus(macAddress string) (model.SwitchBotStatus, error)
	Close()
}

func main() {
	logger := common_lib.NewLogger()
	defer logger.Sync()

	c, err := config.NewConfig(logger)
	if err != nil {
		return
	}

	db, err := initDB(c, logger)
	if err != nil {
		return
	}
	defer db.Close()
	logger.Info("connected to DB")

	l, err := shouldTurnLightsOff(db, c, logger)
	if err != nil {
		return
	}
	if !l {
		return
	}

	playMp3(c, logger)

	l, err = shouldTurnLightsOff(db, c, logger)
	if err != nil {
		return
	}
	if !l {
		return
	}

	ss, err := switchbot.NewSwitchBotService(c.Switch.SwitchBotMac, db, c.Switch.SwitchBotScriptPath)
	if err != nil {
		logger.Error(err.Error())
		return
	}
	err = ss.Toggle()
	if err != nil {
		logger.Error(err.Error())
		return
	}
	logger.Info("turned off light")
}

func initDB(c *config.Config, logger *zap.Logger) (LightManager, error) {
	do := &database.DatabaseOption{
		Name:     c.Db.Name,
		User:     c.Db.User,
		Password: c.Db.Password,
		Host:     c.Db.Host,
		Port:     c.Db.Port,
	}
	d, err := database.NewPostgresService(do)
	if err != nil {
		logger.Error(errors.Wrap(err, "failed to init postgres service").Error())
		return nil, err
	}

	err = d.Connect()
	if err != nil {
		logger.Error(errors.Wrap(err, "failed to connect to DB").Error())
		return nil, err
	}

	return d, nil
}

func shouldTurnLightsOff(lm LightManager, c *config.Config, logger *zap.Logger) (bool, error) {
	// 指定時間帯でなければfalse
	now, _ := time.Parse("15:04", time.Now().Format("15:04"))
	if now.Before(c.Time.StartTime) {
		logger.Info("before light management time", zap.String("light_management_start", c.Time.StartTime.Format("15:04")))
		return false, nil
	}
	if now.After(c.Time.EndTime) {
		logger.Info("after light management time", zap.String("light_management_end", c.Time.EndTime.Format("15:04")))
		return false, nil
	}

	if now.Before(c.Time.LightOffTime) {
		mins, err := lm.GetLightedMinsAfter(c.Time.StartTime, c.Switch.LuxThreshold)
		if err != nil {
			logger.Error(errors.Wrap(err, "failed to get lighted mins from DB").Error())
			return false, err
		}
		if mins < c.Time.LightOnMin {
			logger.Info("lighted mins is still in allowed range", zap.String("mins", strconv.Itoa(mins)), zap.String("max", strconv.Itoa(c.Time.LightOnMin)))
			return false, nil
		}
	} else {
		logger.Info("light off time")
	}

	rpz, err := lm.GetLatestRpzSensorLog()
	if err != nil {
		logger.Error(errors.Wrap(err, "failed to get latest rpz sensor log").Error())
		return false, err
	}
	if rpz.Lux < float64(c.Switch.LuxThreshold) {
		logger.Info("already dark enough")
		return false, nil
	}

	logger.Info("time to turn off light")

	return true, nil
}

func playMp3(c *config.Config, logger *zap.Logger) error {
	mp3, err := os.Open(c.Mp3.Mp3Path)
	if err != nil {
		logger.Error(errors.Wrap(err, "failed to open mp3").Error(), zap.String("mp3", c.Mp3.Mp3Path))
		return err
	}
	defer mp3.Close()

	dec, err := minimp3.NewDecoder(mp3)
	if err != nil {
		logger.Error(errors.Wrap(err, "failed to decode mp3").Error(), zap.String("mp3", c.Mp3.Mp3Path))
		return err
	}
	defer dec.Close()
	<-dec.Started()

	var context *oto.Context
	if context, err = oto.NewContext(dec.SampleRate, dec.Channels, 2, 1024); err != nil {
		logger.Error(errors.Wrap(err, "failed to new oto context").Error())
		return err
	}
	var player = context.NewPlayer()
	defer player.Close()

	logger.Info("started mp3")
	io.Copy(player, dec)

	return nil
}
