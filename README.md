# light_management_night

- 夜部屋の電気をoffにする

## セットアップ

- DB初期化
  - リンク先のスクリプト実行
    - `https://gitlab.com/home_life_management/common_lib/-/blob/main/init/setup.sh`

## 処理の流れ

1. 19:00過ぎると毎分下記実行
1. 19:00以降で明度10以上の分数を取得
1. 分数が30以上であればmp3を2分以上再生
1. mp3再生後、明度取得
1. 明度が10以上であればswitchbot押下
