package config

import (
	"os"
	"time"

	"github.com/creasty/defaults"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

type DatabaseConfig struct {
	Name     string `mapstructure:"name"`
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
	Host     string `default:"localhost" mapstructure:"host"`
	Port     int    `default:"5432" mapstructure:"port"`
}

type SwitchConfig struct {
	LuxThreshold        int    `default:"10" mapstructure:"lux-threshold"`
	SwitchBotMac        string `mapstructure:"switch-bot-mac"`
	SwitchBotScriptPath string `mapstructure:"switch-bot-script-path"`
}

type TimeConfig struct {
	StartTime    time.Time // default値は19:00だが、直接文字列をmapできないため、別途map
	EndTime      time.Time // default値は23:59だが、直接文字列をmapできないため、別途map
	LightOffTime time.Time // 無条件に消灯する時間。default値は22:00だが、直接文字列をmapできないため、別途map
	LightOnMin   int       `default:"30" mapstructure:"light-on-min"` // EndTime以降に点灯状態を許容する分数
}

type Mp3Config struct {
	Mp3Path string `mapstructure:"path"`
}

type Config struct {
	Db     DatabaseConfig `mapstructure:"database"`
	Switch SwitchConfig   `mapstructure:"switch"`
	Time   TimeConfig     `mapstructure:"time"`
	Mp3    Mp3Config      `mapstructure:"mp3"`

	logger *zap.Logger
}

func NewConfig(l *zap.Logger) (*Config, error) {
	c := &Config{
		logger: l,
	}

	err := defaults.Set(c)
	if err != nil {
		err = errors.Wrap(err, "failed to set default value to Config")
		c.logger.Error(err.Error())
		return nil, err
	}
	// time.Time型のStartTime, EndTimeにdefaultsライブラリで値を設定できないためここで設定
	start, _ := time.Parse("15:04", "19:00")
	c.Time.StartTime = start
	end, _ := time.Parse("15:04", "23:59")
	c.Time.EndTime = end
	lightOffTime, _ := time.Parse("15:04", "22:00")
	c.Time.LightOffTime = lightOffTime

	err = c.readConfigToml()
	if err != nil {
		return nil, err
	}

	err = c.checkRequiredParams()
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (c *Config) readConfigToml() error {
	t, err := c.getTomlPath()
	if err != nil {
		return err
	}

	v := viper.New()
	v.SetConfigFile(t)
	err = v.ReadInConfig()
	if err != nil {
		err = errors.Wrap(err, "failed to read setting toml file")
		c.logger.Error(err.Error(), zap.String("file", t))
		return err
	}

	err = c.unmarshalConfig(v)
	if err != nil {
		return err
	}

	return nil
}

func (c *Config) getTomlPath() (string, error) {
	t := os.Getenv("TOML_PATH")
	if _, err := os.Stat(t); err != nil {
		err = errors.Wrap(err, "failed to get config toml path")
		c.logger.Error(err.Error(), zap.String("path", t))
		return "", err
	}

	return t, nil
}

func (c *Config) unmarshalConfig(v *viper.Viper) error {
	err := v.Unmarshal(c)
	if err != nil {
		err = errors.Wrap(err, "failed to unmarshal setting toml file")
		c.logger.Error(err.Error())
		return err
	}

	// time.Time型のStartTimeはviperで直接mapできないため、ここで対応
	var startTime string
	err = v.UnmarshalKey("time.start-time", &startTime)
	if err != nil {
		err = errors.Wrap(err, "failed to unmarshal start-time in config")
		c.logger.Error(err.Error())
		return err
	}
	if startTime != "" {
		start, _ := time.Parse("15:04", startTime)
		c.Time.StartTime = start
	}

	// time.Time型のEndTimeはviperで直接mapできないため、ここで対応
	var endTime string
	err = v.UnmarshalKey("time.end-time", &endTime)
	if err != nil {
		err = errors.Wrap(err, "failed to unmarshal end-time in config")
		c.logger.Error(err.Error())
		return err
	}
	if endTime != "" {
		end, _ := time.Parse("15:04", endTime)
		c.Time.EndTime = end
	}

	// time.Time型のLightOffTimeはviperで直接mapできないため、ここで対応
	var lightOffTime string
	err = v.UnmarshalKey("time.light-off-time", &lightOffTime)
	if err != nil {
		err = errors.Wrap(err, "failed to unmarshal light-off-time in config")
		c.logger.Error(err.Error())
		return err
	}
	if lightOffTime != "" {
		l, _ := time.Parse("15:04", lightOffTime)
		c.Time.LightOffTime = l
	}

	return nil
}

func (c *Config) checkRequiredParams() error {
	if c.Db.Name == "" {
		err := errors.New("required parameter(database.db-name) not specified")
		c.logger.Error(err.Error())
		return err
	} else if c.Db.User == "" {
		err := errors.New("required parameter(database.db-user) not specified")
		c.logger.Error(err.Error())
		return err
	} else if c.Db.Password == "" {
		err := errors.New("required parameter(database.db-password) not specified")
		c.logger.Error(err.Error())
		return err
	} else if c.Switch.SwitchBotMac == "" {
		err := errors.New("required parameter(switch.switch-bot-mac) not specified")
		c.logger.Error(err.Error())
		return err
	} else if c.Switch.SwitchBotScriptPath == "" {
		err := errors.New("required parameter(switch.switch-bot-script-path) not specified")
		c.logger.Error(err.Error())
		return err
	} else if c.Mp3.Mp3Path == "" {
		err := errors.New("required parameter(mp3.path) not specified")
		c.logger.Error(err.Error())
		return err
	}

	return nil
}
